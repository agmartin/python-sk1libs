#!/usr/bin/env python
#
# Setup script for sk1libs
#
# Copyright (c) 2009 Igor E. Novikov
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
# Usage: 
# --------------------------------------------------------------------------
#  to build package:   python setup.py build
#  to install package:   python setup.py install
# --------------------------------------------------------------------------
#  to create source distribution:   python setup.py sdist
# --------------------------------------------------------------------------
#  to create binary RPM distribution:  python setup.py bdist_rpm
#
#  to create deb package just use alien command (i.e. rpm2deb)
#
#  help on available distribution formats: python setup.py bdist --help-formats
#


import os, sys, struct

COPY=False

if __name__ == "__main__":
	if len(sys.argv)>1 and sys.argv[1]=='build&copy':
		COPY=True
		sys.argv[1]='build'
	
	from distutils.core import setup, Extension
		
	share_dirs=[]
	for item in ['GNU_LGPL_v2', 'COPYRIGHTS']:
		share_dirs.append(item)
 
		
 	pycms_src='src/pycms/'			
	pycms_module = Extension('sk1libs.pycms._pycms',
			define_macros = [('MAJOR_VERSION', '1'),
						('MINOR_VERSION', '0')],
			sources = [pycms_src+'_pycms.c'],
			libraries=['lcms','kernel32', 'user32', 'gdi32'],
			extra_compile_args=["-Wall"])
			
 	ft2_src='src/ft2engine/libft2/'				
	ft2_module = Extension('sk1libs.ft2engine.ft2',
			define_macros = [('MAJOR_VERSION', '1'),
						('MINOR_VERSION', '0'),
						('_WIN32_WINNT',None),('__WINDOWS__',None)],
			sources = [ft2_src+'ft2module.c'],
			libraries=['freetype','kernel32', 'user32', 'gdi32'],
			extra_compile_args=["-Wall"])
			
 	immath_src='src/imaging/libimagingmath/'			
	immath_module = Extension('sk1libs.imaging._imagingmath',
			define_macros = [('MAJOR_VERSION', '1'),
						('MINOR_VERSION', '0')],
			sources = [immath_src+'_imagingmath.c'],
			extra_compile_args=["-Wall"])
			
 	imcms_src='src/imaging/libimagingcms/'			
	imcms_module = Extension('sk1libs.imaging._imagingcms',
			define_macros = [('MAJOR_VERSION', '1'),
						('MINOR_VERSION', '0')],
			sources = [imcms_src+'_imagingcms.c'],
			libraries=['lcms','kernel32', 'user32', 'gdi32'],
			extra_compile_args=["-Wall"])
			
 	imft_src='src/imaging/libimagingft/'			
	imft_module = Extension('sk1libs.imaging._imagingft',
			define_macros = [('MAJOR_VERSION', '1'),
						('MINOR_VERSION', '0')],
			sources = [imft_src+'_imagingft.c'],
			libraries=['freetype'],
			extra_compile_args=["-Wall"])
			
	defs = [('MAJOR_VERSION', '1'), ('MINOR_VERSION', '0'), ("HAVE_LIBJPEG", None), ("HAVE_LIBZ", None)]
	if struct.unpack("h", "\0\1")[0] == 1:
		defs.append(("WORDS_BIGENDIAN", None))
	
 	imaging_src='src/imaging/libimaging/'			
	imaging_module = Extension('sk1libs.imaging._imaging',
			sources = [imaging_src+'Access.c', imaging_src+'Antialias.c', imaging_src+'Bands.c', imaging_src+'BitDecode.c', 
					imaging_src+'Blend.c', imaging_src+'Chops.c', imaging_src+'Convert.c', imaging_src+'ConvertYCbCr.c', 
					imaging_src+'Copy.c', imaging_src+'Crc32.c', imaging_src+'Crop.c', imaging_src+'decode.c', 
					imaging_src+'Dib.c', imaging_src+'display.c', imaging_src+'Draw.c', imaging_src+'Effects.c', 
					imaging_src+'encode.c', imaging_src+'EpsEncode.c', imaging_src+'File.c', #imaging_src+'Except.c', 
					imaging_src+'Fill.c', imaging_src+'Filter.c', imaging_src+'FliDecode.c', imaging_src+'Geometry.c', 
					imaging_src+'GetBBox.c', imaging_src+'GifDecode.c', imaging_src+'GifEncode.c', imaging_src+'HexDecode.c', 
					imaging_src+'Histo.c', imaging_src+'_imaging.c', imaging_src+'JpegDecode.c', imaging_src+'JpegEncode.c', 
					imaging_src+'LzwDecode.c', imaging_src+'map.c', imaging_src+'Matrix.c', imaging_src+'ModeFilter.c', 
					imaging_src+'MspDecode.c', imaging_src+'Negative.c', imaging_src+'Offset.c', imaging_src+'outline.c', 
					imaging_src+'Pack.c', imaging_src+'PackDecode.c', imaging_src+'Palette.c', imaging_src+'Paste.c', 
					imaging_src+'path.c', imaging_src+'PcdDecode.c', imaging_src+'PcxDecode.c', imaging_src+'PcxEncode.c', 
					imaging_src+'Point.c', imaging_src+'Quant.c', imaging_src+'QuantHash.c', imaging_src+'QuantHeap.c', 
					imaging_src+'RankFilter.c', imaging_src+'RawDecode.c', imaging_src+'RawEncode.c', imaging_src+'Storage.c', 
					imaging_src+'SunRleDecode.c', imaging_src+'TgaRleDecode.c', imaging_src+'Unpack.c', imaging_src+'UnpackYCC.c', 
					imaging_src+'UnsharpMask.c', imaging_src+'XbmDecode.c', imaging_src+'XbmEncode.c', imaging_src+'ZipDecode.c', 
					imaging_src+'ZipEncode.c', ],
			libraries=['jpeg','zlib','kernel32', 'user32', 'gdi32'], #,'tiff' TIFF patch will be later
			define_macros=defs,
			extra_compile_args=["-Wall"])
			
	setup (name = 'sk1libs',
			version = '0.9.1',
			description = 'sk1libs is a set of python non-GUI extensions for sK1 Project',
			author = 'Igor E. Novikov',
			author_email = 'igor.e.novikov@gmail.com',
			maintainer = 'Igor E. Novikov',
			maintainer_email = 'igor.e.novikov@gmail.com',
			license = 'LGPL v2',
			url = 'http://sk1project.org',
			download_url = 'http://sk1project.org/',
			long_description = '''sk1libs is a set of python non-GUI extensions for sK1 Project. The package includes multiplatform non-GUI extensions which are usually native extensions. sK1 Team (http://sk1project.org), copyright (c) 2009 by Igor E. Novikov.
			''',
		classifiers=[
			'Development Status :: 5 - Stable',
			'Environment :: Desktop',
			'Environment :: Console',
			'Environment :: Web Environment',
			'Intended Audience :: End Users/Desktop',
			'Intended Audience :: Developers',
			'License :: OSI Approved :: LGPL v2',
			'Operating System :: POSIX',
			'Operating System :: Microsoft :: Windows',
			'Operating System :: MacOS :: MacOS X',
			'Programming Language :: Python',
			'Programming Language :: C',
			"Topic :: Multimedia :: Graphics",
			],

		packages = ['sk1libs',
				'sk1libs.pycms',
				'sk1libs.ft2engine',
				'sk1libs.imaging',
				'sk1libs.libpdf',
				'sk1libs.libpdf.lib',
				'sk1libs.libpdf.pdfbase',
				'sk1libs.libpdf.pdfgen',
				'sk1libs.utils',
				'sk1libs.filters',
				'sk1libs.filters.formats',
				],

			
		package_dir = {'sk1libs': 'src',
		'sk1libs.pycms': 'src/pycms',
		'sk1libs.ft2engine': 'src/ft2engine',
		'sk1libs.imaging': 'src/imaging',
		'sk1libs.libpdf': 'src/libpdf',
		'sk1libs.libpdf.lib': 'src/libpdf/lib',
		'sk1libs.libpdf.pdfbase': 'src/libpdf/pdfbase',
		'sk1libs.libpdf.pdfgen': 'src/libpdf/pdfgen',
		'sk1libs.utils': 'src/utils',
		'sk1libs.filters': 'src/filters',
		'sk1libs.filters.formats': 'src/filters/formats',
		},
			
		package_data = {'sk1libs.filters': ['import/*.py','export/*.py','parsing/*.py','preview/*.py'], 
					'sk1libs.ft2engine': ['fallback_fonts/*.*'],'sk1libs.pycms': ['profiles/*.*'],},
		
		ext_modules = [pycms_module, imaging_module, immath_module, 
					imcms_module, imft_module, ft2_module])
	
##############################################
# This section for developing purpose only
# Command 'python setup.py build&copy' allows
# automating build and native extension copying
# into package directory
##############################################
if COPY:
	import shutil, string, platform
	version=(string.split(sys.version)[0])[0:3]
	
	shutil.copy('build/lib.linux-'+platform.machine()+'-'+version+'/sk1libs/pycms/_pycms.so','src/pycms/')
	print '\n _pycms.so has been copied to src/ directory'

	shutil.copy('build/lib.linux-'+platform.machine()+'-'+version+'/sk1libs/imaging/_imaging.so','src/imaging/')
	print '\n _imaging.so has been copied to src/ directory'
	
	shutil.copy('build/lib.linux-'+platform.machine()+'-'+version+'/sk1libs/imaging/_imagingmath.so','src/imaging/')
	print '\n _imagingmath.so has been copied to src/ directory'
	
	shutil.copy('build/lib.linux-'+platform.machine()+'-'+version+'/sk1libs/imaging/_imagingcms.so','src/imaging/')
	print '\n _imagingcms.so has been copied to src/ directory'
	
	shutil.copy('build/lib.linux-'+platform.machine()+'-'+version+'/sk1libs/imaging/_imagingft.so','src/imaging/')
	print '\n _imagingft.so has been copied to src/ directory'
	
	shutil.copy('build/lib.linux-'+platform.machine()+'-'+version+'/sk1libs/ft2engine/ft2.so','src/ft2engine/')
	print '\n ft2.so has been copied to src/ directory'
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
